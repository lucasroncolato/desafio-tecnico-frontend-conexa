import React, { useState } from "react";
import { ThemeProvider } from "styled-components";
import theme from "./mainTheme";
import GlobalStyle from "./GlobalStyle";
import Login from "./pages/Login";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Home from "./pages/Home";
import StyledScheduling from "./pages/Scheduling";
import PrivateRoute from "./atoms/PrivateRoute";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <BrowserRouter>
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <PrivateRoute path="/home">
            <Home />
          </PrivateRoute>
          <PrivateRoute path="/scheduling">
            <StyledScheduling />
          </PrivateRoute>
          <Route path="*">
            <Redirect to="/login" />
          </Route>
        </Switch>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
