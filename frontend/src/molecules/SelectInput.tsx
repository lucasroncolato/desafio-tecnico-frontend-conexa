import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Typography from "../atoms/Typography";

interface Props {
  onChange: (event: React.ChangeEvent<HTMLSelectElement>) => void;
  value: string | number;
  name?: string;
  className?: string;
  title?: string;
  placeholder?: string;
  children?: React.ReactElement | React.ReactElement[];
}

function SelectInput(props: Props) {
  return (
    <div className={props.className}>
      <div className={"SelectInputContainer"}>
        <div className="SelectInputTitleContainer">
          <Typography>{props.title}</Typography>
        </div>
        <div className="SelectContainer">
          <select name={props.name} onChange={props.onChange}>
            {props.children}
          </select>
        </div>
      </div>
    </div>
  );
}

const StyledSelectInput = styled(SelectInput)<Props>`
  .SelectInputContainer {    
    max-width: 235px;

    & .SelectInputTitleContainer {
      display: flex;
      flex-direction: row;
      text-align: initial;
      align-items: center;
    }

    & .SelectContainer {
      display: flex;
      flex-grow: 1;
      color: ${(props) => props.theme.colors.text.content};
      border: none;
      &:focus-visible {
        outline: none;
      }
      & select {
        border-radius: 1em;
        padding: 1em;
        cursor: pointer;
        size: 14px;
        color: #2E50D4;
        background-color: #FFFFFB;
        border: 2px solid #2E50D4;
        flex-grow: 1;
        margin-top: 15px;
      }

      & option {
        border-radius: 1em !important;
        padding: 1em !important;
        cursor: pointer !important;
        size: 14px !important;
        color: #2E50D4 !important;
        background-color: #FFFFFB !important;
        border: 2px solid #2E50D4 !important;
      }
    }
  }
`;

export default StyledSelectInput;
