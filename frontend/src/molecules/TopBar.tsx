import React from "react";
import { useHistory } from "react-router";
import styled from "styled-components";
import logo from "../assets/logo-conexa.png";

interface Props {
  label?: React.ReactChild;
  actionButton?: React.ReactChild;
  className?: string;
}

function TopBar(props: Props) {
  const history = useHistory();
  return (
    <div className={props.className}>
      <img
        src={logo}
        alt="Logo"
        onClick={() => {
          history.push("/home");
        }}
      />
      <div className="actionContainer">
        <div className="labelContainer">{props.label}</div>
        <div className="buttonContainer">{props.actionButton}</div>
      </div>
    </div>
  );
}

const StyledTopBar = styled(TopBar)<Props>`
  box-shadow: 0px 3px 5px lightgrey;
  display: flex;
  width: 100vw;
  padding: 16px 12px;
  justify-content: space-between;
  align-items: center;
  box-sizing: border-box;
  min-height: 80px;
  & img {
    cursor: pointer;
    width: 141px;
    height: 23px;
  }
  & .actionContainer {
    display: flex;
    align-items: center;
    & .labelContainer {
      padding-right: 15px;
      @media (max-width: 768px) {
        display: none;
      }
    }
  }
  @media (max-width: 768px) {
    justify-content: ${(props) =>
      props.actionButton ? "space-between" : "center"};
  }
`;
export default StyledTopBar;
