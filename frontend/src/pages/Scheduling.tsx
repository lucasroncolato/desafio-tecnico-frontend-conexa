import React, { useEffect, useState } from "react";
import Button from "../atoms/Button";
import IlustrationPlant from "../assets/illustration-plant.png";
import IlustrationCertificates from "../assets/illustration-certificates.png";
import Typography from "../atoms/Typography";
import styled from "styled-components";
import StyledBasicTemplate from "../templates/BasicTemplate";
import axios, { AxiosResponse } from "axios";
import StyledSelectInput from "../molecules/SelectInput";
import { useHistory } from "react-router";

interface Props {
  className?: string;
}
type Patients = {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
}[];

function Scheduling(props: Props) {
  const [patients, setPatients] = useState<Patients>([]);
  const [patientId, setPatientId] = useState(0);
  const [schedulingDate, setSchedulingDate] = useState(new Date());
  const token = window.localStorage.getItem("token");
  const history = useHistory();

  useEffect(() => {
    axios("http://localhost:3333/patients", {
      method: "GET",
      headers: { authorization: token },
    })
      .then((response: AxiosResponse<Patients>) => {
        setPatients(response.data);
        setPatientId(response.data[0].id);
      })
      .catch((error) =>
        console.log("Não foi possivel carregar os itens", error)
      );
  }, []);

  function saveInfo() {
    axios("http://localhost:3333/consultations", {
      method: "POST",
      headers: { authorization: token },
      data: {
        patientId: patientId,
        date: schedulingDate,
      },
    })
      .then((_response) => history.push("/home"))
      .catch((error) => console.log(error));
  }

  return (
    <StyledBasicTemplate title="Agendamento">
      <div className={props.className}>
        <div className="formContainer">
          <div className="selectPatient">
            <StyledSelectInput
              value={patientId}
              onChange={(value) => console.log(value.target.value)}
              title="Paciente"
              placeholder="Selecione o paciente"
            >
              {patients.length > 0
                ? patients.map((patient) => (
                  <option
                    value={patient.id}
                    key={patient.id}
                  >{`${patient.first_name} ${patient.last_name}`}</option>
                ))
                : undefined}
            </StyledSelectInput>
          </div>
          <div className="selectDate">
            <Typography>Data</Typography>
            <input
              type="datetime-local"
              value={new Date(
                schedulingDate.valueOf() -
                schedulingDate.getTimezoneOffset() * 60000
              )
                .toISOString()
                .substring(0, 16)}
              onChange={(value) =>
                setSchedulingDate(new Date(value.target.value))
              }
            />
          </div>
        </div>
        <div className="buttonContainer">
          <Button onClick={() => saveInfo()}>Gravar</Button>
        </div>
      </div>
    </StyledBasicTemplate>
  );
}

const StyledScheduling = styled(Scheduling)`
  height: 100%;
  width: 100%;
  max-height: 360px;
  max-width: 520px;
  display: flex;
  flex-direction: column;
  & .formContainer {
    display: flex;
    flex-direction: row;
    & .selectPatient {
      flex-grow: 1;
    }
    & .selectDate {
      display: flex;
      flex-direction: column;
      max-width: 235px;
      & input {
        border-radius: 1em;
        padding: 1em;
        cursor: pointer;
        size: 14px;
        color: #2E50D4;
        background-color: #FFFFFB;
        border: 2px solid #2E50D4;
        margin-top: 15px;
      }
    }
    @media (max-width: 768px) {
      flex-direction: column;
      align-items: center;
    }
  }
  & .buttonContainer {
    margin: 15px;
    display: flex;
    height: 100%;
    align-items: flex-end;
    justify-content: flex-end;
  }
`;

export default StyledScheduling;
