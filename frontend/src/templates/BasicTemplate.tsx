import React, { useEffect } from "react";
import Button from "../atoms/Button";
import StyledTopBar from "../molecules/TopBar";
import Typography from "../atoms/Typography";
import styled from "styled-components";
import { useHistory } from "react-router";

interface Props {
  className?: string;
  title: String;
  children?: React.ReactElement;
}

function BasicTemplate(props: Props) {
  const userName = window.localStorage.getItem("name");
  const history = useHistory();
  function logoff() {
    window.localStorage.clear();
    history.push("/");
  }

  return (
    <div className={props.className}>
      <StyledTopBar
        label={`Olá, Dr. ${userName}`}
        actionButton={
          <Button color="secondary" onClick={logoff}>
            Sair
          </Button>
        }
      />
      <div className="pageContainer">
        <Typography type="title">{props.title}</Typography>
        <div className="contentContainer">{props.children}</div>
      </div>
      <div className="footerContainer">
        <Button color="secondary">Ajuda</Button>
        <Button color="primary" onClick={() => history.push("/scheduling")}>
          Agendar consulta
        </Button>
      </div>
    </div>
  );
}

const StyledBasicTemplate = styled(BasicTemplate)`
  display: flex;
  flex-direction: column;
  height: 100vh;
  & .pageContainer {
    flex-grow: 1;
    padding: 2rem;
    overflow: auto;
    display: flex;
    flex-direction: column;
    & .contentContainer {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      flex-grow: 1;
      height: 100%;
      width: 100%;
    }
  }
  & .footerContainer {
    display: flex;
    justify-content: space-between;
    padding: 1rem 2rem;
    @media (max-width: 768px) {
      border-top: 1px solid ${(props) => props.theme.colors.text.content};
    }
  }
`;

export default StyledBasicTemplate;
